#include <algorithm>
#include <memory>
#include <string>
#include <cstdio>

#include "mediametadataretriever_impl.hpp"
#include "ffmpeg_utils.hpp"

#ifdef __cplusplus
extern "C" {
#endif
#include "libavutil/imgutils.h"
#include "libavutil/frame.h"
#include "libavcodec/avcodec.h"
#include "libavutil/avassert.h"
#ifdef __cplusplus
};
#endif

namespace fmmr
{
MediaMetadataRetriever::MediaMetadataRetrieverImpl::MediaMetadataRetrieverImpl()
    :fmt_ctx_(nullptr),
    video_dec_ctx_(nullptr),
    video_stream_(nullptr),
    video_codec_(nullptr),
    video_stream_idx_(AVERROR_STREAM_NOT_FOUND),
    width_(-1),
    height_(-1),
    pix_fmt_(AV_PIX_FMT_NONE)
{
    // Destory();
}

MediaMetadataRetriever::MediaMetadataRetrieverImpl::~MediaMetadataRetrieverImpl()
{
    Destory();
}

int MediaMetadataRetriever::MediaMetadataRetrieverImpl::GetFrameAtTime(int64_t time_in_seconds, uint8_t* const* data, int* linesize, int width, int height)
{
    av_assert0(fmt_ctx_ != nullptr);
    av_assert0(video_dec_ctx_ != nullptr) ;

    av_assert0((width&1) == 0) ;
    av_assert0((height&1) == 0) ;

    av_assert0(width > 2) ;
    av_assert0(height > 2) ;


    int ret = 0;
    AVFrame* frame = nullptr;
    AVPacket* pkt = nullptr;
    bool read_frame = true;

    if (time_in_seconds < 0) {
        time_in_seconds = 0;
    }
    std::int64_t timestamp = AV_TIME_BASE * static_cast<std::int64_t>(time_in_seconds);
    double other_timebase = av_q2d(video_stream_->time_base);
    // std::int64_t real_timestamp = av_rescale_q(time_in_seconds, video_stream_->time_base, (AVRational){AV_TIME_BASE, 1});
     ret = av_seek_frame(fmt_ctx_, video_stream_idx_, static_cast<std::int64_t>(av_rescale_q(timestamp, AV_TIME_BASE_Q, video_stream_->time_base)), AVSEEK_FLAG_BACKWARD);
    //ret = av_seek_frame(fmt_ctx_, -1, timestamp + fmt_ctx_->start_time, AVSEEK_FLAG_BACKWARD);
    if (ret >= 0) {
        avcodec_flush_buffers(video_dec_ctx_);
    } else {
        return -1;
    }

    frame = av_frame_alloc();
    if (!frame) {
        ret = AVERROR(ENOMEM);
        goto end;
    }

    pkt = av_packet_alloc();
    if (!pkt) {
        ret = AVERROR(ENOMEM);
        goto end;
    }

    while (1) {
        pkt->data = NULL;
        pkt->size = 0;
        if (read_frame)
        {
            if (av_read_frame(fmt_ctx_, pkt) < 0)
                break;
        }
        //av_log(video_dec_ctx_, AV_LOG_ERROR, "pkt->stream_index: %d -- %d, size: %d, flags: %d\n", video_stream_idx_, pkt->stream_index, pkt->size, pkt->flags);
        ret = DecodePacket(video_dec_ctx_, pkt, frame);
        //av_log(video_dec_ctx_, AV_LOG_ERROR, "DecodePacket ret: %d\n", ret);
        if (pkt->flags & AV_PKT_FLAG_KEY )
            read_frame = false;
        av_packet_unref(pkt);

        if (ret < 0)
            break;
        if (ret ==1 ) {
            ret = ScaleFrame(frame, data, linesize, width, height, AV_PIX_FMT_BGR24);
            if (ret >= 0) {
                break;
            } else {
                goto end;
            }
        }
        av_frame_unref(frame);
    }
end:

    av_packet_free(&pkt);
    av_frame_free(&frame);
    return ret;
}

int MediaMetadataRetriever::MediaMetadataRetrieverImpl::SetDataSource(const char* videopath)
{
    if (videopath == nullptr) {
        return -1;
    }


    int ret = 0;
    AVDictionary *options = nullptr;

    av_dict_set(&options, "icy", "1", 0);
    av_dict_set(&options, "user-agent", "fmmr", 0);
    av_dict_set(&options, "rtsp_transport", "tcp", 0);

    ret = avformat_open_input(&fmt_ctx_, videopath, nullptr, &options);
    // av_dict_free(&options);
    if (ret < 0) {
        Destory();
        return ret;
    }

    fmt_ctx_->max_analyze_duration = AV_TIME_BASE << 2;
    if (avformat_find_stream_info(fmt_ctx_, NULL) < 0) {
        Destory();
        return -1;
    }

    for (int i=0; i<fmt_ctx_->nb_streams; i++) {
        AVStream *st = fmt_ctx_->streams[i];
        if (st->codecpar->codec_type == AVMEDIA_TYPE_VIDEO && video_stream_idx_ == AVERROR_STREAM_NOT_FOUND)
            video_stream_idx_  = i;
        else
            st->discard = AVDISCARD_ALL;
    }

     if (video_stream_idx_ == AVERROR_STREAM_NOT_FOUND) {
        Destory();
        return ret;
    }

    ret = OpenDecodeCodecContext(&video_stream_idx_, &video_dec_ctx_, &video_codec_, fmt_ctx_, AVMEDIA_TYPE_VIDEO);
    if (ret < 0 || 
        (!video_codec_) || 
        fmt_ctx_->streams[video_stream_idx_] == nullptr) {
        Destory();
        return ret;
    } 
    video_stream_ = fmt_ctx_->streams[video_stream_idx_];
    width_ = video_stream_->codecpar->width;
    height_ = video_stream_->codecpar->height;
    pix_fmt_ = (enum AVPixelFormat) video_stream_->codecpar->format;

    // set_shutcast_metadata(fmt_ctx_);
    InitDict(fmt_ctx_, video_stream_);
    return 0;
}

const char* MediaMetadataRetriever::MediaMetadataRetrieverImpl::ExtractMetadata(const char* utf8_key)
{
    if (utf8_key == nullptr) {
        return "";
    }

    if (fmt_ctx_ == nullptr || fmt_ctx_->metadata == nullptr) {
        return "";
    }

    AVDictionaryEntry* entry = av_dict_get(fmt_ctx_->metadata, utf8_key, nullptr, AV_DICT_MATCH_CASE);
    if (entry == nullptr || entry->value == nullptr) {
        return "";
    }
    return entry->value;
}

/*private static*/
void MediaMetadataRetriever::MediaMetadataRetrieverImpl::InitDict(AVFormatContext* fmt_ctx, AVStream* avstream)
{
    set_duration(fmt_ctx, avstream);
    set_filesize(fmt_ctx);
    set_framerate(fmt_ctx, avstream);
    set_rotation(fmt_ctx, avstream);
    set_video_dimensions(fmt_ctx, avstream);
}


/*private*/
void MediaMetadataRetriever::MediaMetadataRetrieverImpl::Destory()
{
    avcodec_free_context(&video_dec_ctx_);
    avformat_close_input(&fmt_ctx_);

    video_stream_ = nullptr;
    video_stream_idx_ = AVERROR_STREAM_NOT_FOUND;
    width_ = -1;
    height_ = -1;
    pix_fmt_ = AV_PIX_FMT_NONE;
}


int MediaMetadataRetriever::MediaMetadataRetrieverImpl::DecodePacket(AVCodecContext* dec_ctx, const AVPacket* dec_pkt, AVFrame* frame)
{
    av_assert0(dec_ctx != nullptr) ;
    av_assert0(dec_pkt != nullptr) ;
    av_assert0(frame != nullptr) ;

    int ret = avcodec_send_packet(dec_ctx, dec_pkt);
    if (ret < 0)
        return ret;

    ret = avcodec_receive_frame(dec_ctx, frame);
    if (ret < 0) {
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
            return 0;
        return ret;
    }
    return 1;
}

int MediaMetadataRetriever::MediaMetadataRetrieverImpl::OpenDecodeCodecContext(int *stream_idx, AVCodecContext** dec_ctx, AVCodec** decoder, AVFormatContext *fmt_ctx, enum AVMediaType type)
{
    int ret;
    AVStream* st;

    st = fmt_ctx->streams[*stream_idx];
    *decoder = avcodec_find_decoder(st->codecpar->codec_id);

    if (st == nullptr) {
        return -1;
    }

    if (*decoder == nullptr) {
        return AVERROR(EINVAL);
    }

    *dec_ctx = avcodec_alloc_context3(*decoder);

    if (*dec_ctx == nullptr) {
        return AVERROR(ENOMEM);
    }

    if ((ret = avcodec_parameters_to_context(*dec_ctx, st->codecpar)) < 0) {
        return ret;
    }

    if ((ret = avcodec_open2(*dec_ctx, *decoder, nullptr)) < 0) {
        return ret;
    }


    return 0;
}

int MediaMetadataRetriever::MediaMetadataRetrieverImpl::ScaleFrame(const AVFrame* iframe, 
                                                                    uint8_t* const* dst_data, int* dst_linesize, 
                                                                    int dst_width, int dst_height, enum AVPixelFormat dst_pix_fmt)
{
    av_assert0(iframe != nullptr) ;
    av_assert0(dst_data != nullptr) ;
    av_assert0(dst_linesize != nullptr) ;
    av_assert0(iframe->format != -1) ;

    SwsContext* sws_ctx = sws_getContext(iframe->width, iframe->height, (enum AVPixelFormat)iframe->format,
                                        dst_width, dst_height, dst_pix_fmt, 
                                        SWS_POINT, nullptr, 
                                        nullptr, nullptr);
    if (!sws_ctx) {
        return -1;
    }
    sws_scale(sws_ctx, (const uint8_t* const *)iframe->data, iframe->linesize,
              0, iframe->height, dst_data, dst_linesize);
    sws_freeContext(sws_ctx);
	
    return 0;
}

}; //namespace fmmr
