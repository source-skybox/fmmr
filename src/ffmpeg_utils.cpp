
#include "ffmpeg_utils.hpp"

#ifdef __cplusplus
extern "C" {
#endif
#include "libavformat/avformat.h"
#include "libavutil/avutil.h"
#include "libavutil/opt.h"
#ifdef __cplusplus
};
#endif

#include <string>

namespace fmmr
{

void set_duration(AVFormatContext* ic, AVStream* avstream)
{
    RETURN_IF_FAILED(ic != nullptr, "ic is nullptr");

    RETURN_IF_FAILED(avstream != nullptr, "avstream is nullptr");

    int64_t duration = 0;

    /*
    if (avstream->duration != AV_NOPTS_VALUE && avstream->time_base.num && avstream->time_base.gen) {
        duration = avstream->duration * av_q2d(avstream->time_base);
    }
    */

    if (ic->duration != AV_NOPTS_VALUE) {
        // duration = static_cast<int64_t>(ic->duration * av_q2d(avstream->time_base));
        duration = ic->duration + (ic->duration <= INT64_MAX - 5000 ? 5000 : 0);
        duration /= AV_TIME_BASE;
    }
    av_dict_set(&ic->metadata, DURATION, std::to_string(duration).c_str(), 0);
}

void set_framerate(AVFormatContext* ic, AVStream* avstream)
{
    RETURN_IF_FAILED(ic != nullptr, "ic is nullptr");

    RETURN_IF_FAILED(avstream != nullptr, "avstream is nullptr");

    uint64_t framerate = 0;
    if (avstream->avg_frame_rate.num && avstream->avg_frame_rate.den) {
        framerate = static_cast<uint64_t>(av_q2d(avstream->avg_frame_rate));
    }
    av_dict_set(&ic->metadata, FRAMERATE, std::to_string(framerate).c_str(), 0);
}

void set_filesize(AVFormatContext* ic)
{
    RETURN_IF_FAILED(ic != nullptr, "ic is nullptr");

    if (ic->pb != nullptr) {
        size_t size = avio_size(ic->pb);
        av_dict_set(&ic->metadata, FILESIZE, std::to_string(size).c_str(), 0);
    } else {
        av_dict_set(&ic->metadata, FILESIZE, "-1", 0);
    }
}

void set_rotation(AVFormatContext* ic, AVStream* avstream)
{
    RETURN_IF_FAILED(ic != nullptr, "ic is nullptr");

    RETURN_IF_FAILED(avstream != nullptr, "avstream is nullptr");

    AVDictionaryEntry* entry = nullptr;
    if (avstream->metadata != nullptr && (entry = av_dict_get(avstream->metadata, ROTATE, NULL, AV_DICT_MATCH_CASE)) != nullptr) {
        if (entry->value != nullptr) {
            av_dict_set(&ic->metadata, ROTATE, entry->value, 0);
            return;
        }
    }

    if ((entry = av_dict_get(ic->metadata, ROTATE, nullptr, AV_DICT_MATCH_CASE)) == nullptr || entry->value == nullptr) {
        av_dict_set(&ic->metadata, ROTATE, "0", 0);
    }
}

void set_video_dimensions(AVFormatContext *ic, AVStream *avstream)
{
    RETURN_IF_FAILED(ic != nullptr, "ic is nullptr");

    RETURN_IF_FAILED(avstream != nullptr, "avstream is nullptr");

    av_dict_set(&ic->metadata, VIDEO_WIDTH, std::to_string(avstream->codecpar->width).c_str(), 0);
    av_dict_set(&ic->metadata, VIDEO_HEIGHT, std::to_string(avstream->codecpar->height).c_str(), 0);
}

void set_shutcast_metadata(AVFormatContext* ic)
{
    RETURN_IF_FAILED(ic != nullptr, "ic is nullptr");

    char* value = nullptr;

    if (av_opt_get(ic, "icy_metadata_packet", 1, (uint8_t**)&value) < 0) {
        value = nullptr;
    }

    if (value != nullptr && value[0] != '\0') {
        av_dict_set(&ic->metadata, ICY_METADATA, value, 0);
    }
}

}; // namespace fmmr
