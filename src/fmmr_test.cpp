#include <mediametadataretriever.hpp>

#include <opencv2/opencv.hpp>

#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

std::vector<const char*> kMetadataKey = {
    fmmr::VIDEO_WIDTH,
    fmmr::VIDEO_HEIGHT,
    fmmr::DURATION,
    fmmr::FRAMERATE,
    fmmr::FILESIZE
};

int main(int argc, char** argv)
{
    if (argc < 2) {
        std::cout << "help: binary [video path] <thumbnail save path>" << std::endl;
        return EXIT_FAILURE;
    }

    auto retriever = fmmr::MediaMetadataRetriever::MediaMetadataRetrieverCreate();
    if (!retriever) {
        std::cout << "Failed to MediaMetadataRetrieverCreate()" << std::endl;
        return EXIT_FAILURE;
    }

    if (retriever->SetDataSource(argv[1]) != 0) {
        std::cout << "Failed to SetDataSource(" << argv[1] << ")" << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "##TEST Start ExtractMetadata()" << std::endl;
    for (const auto& key : kMetadataKey) {
        const char* value = retriever->ExtractMetadata(key);
        std::cout << key << ": " << value << std::endl;
    }
    std::cout << "##TEST END ExtractMetadata()" << std::endl;

    std::cout << "##TEST Start GetFrameAtTime()" << std::endl;

    if (argc >= 3) {
        std::int64_t duration = std::atoi(retriever->ExtractMetadata(fmmr::DURATION));
        int width = std::atoi(retriever->ExtractMetadata(fmmr::VIDEO_WIDTH));
        int height = std::atoi(retriever->ExtractMetadata(fmmr::VIDEO_HEIGHT));
        cv::Mat img(height, width, CV_8UC3);
        int lines[1];
        lines[0] = img.step1();
        retriever->GetFrameAtTime(duration >> 1, &img.data, lines);
        cv::imwrite(argv[2], img);
    }
    std::cout << "##TEST End GetFrameAtTime()" << std::endl;
    return EXIT_SUCCESS;
}