# 简介
## 1. 目录maintainer
@xwtkdym

## 编译
### 1. window: x86_64
cd src_dir
mkdir build && cd build
cmake .. -DARCH=x86_64 -DCMAKE_TOOLCHAIN_FILE=../toolchain/mingw64-toolchain.cmake

### 2. android: armeabi-v7a
cd src_dir
mkdir build && cd build
cmake .. -DARCH=armeabi-v7a -DCMAKE_TOOLCHAIN_FILE=/your-ndk-path/build/cmake/android.toolchain.cmake
