#ifndef FFMPEG_FFMPEG_UTILS_H_
#define FFMPEG_FFMPEG_UTILS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "libavformat/avformat.h"

#ifdef __cplusplus
};
#endif

#define LOG_TMP(code, message) 

#define RESET_AND_RETURN_CODE_IF_FAILED(pred, code, message) \
    do { \
        if (!(pred)) { \
            Reset(); \
            return (code); \
        } \
    } while(0);

#define RETURN_CODE_IF_FAILED(pred, code, message)  \
    do { \
        if (!(pred)) { \
            return (code); \
        } \
    } while(0);

#define RETURN_IF_FAILED(pred, message) \
    do { \
        if (!(pred)) { \
            return ; \
        } \
    } while(0);


static const char *DURATION = "duration";
static const char *AUDIO_CODEC = "audio_codec";
static const char *VIDEO_CODEC = "video_codec";
static const char *ICY_METADATA = "icy_metadata";
static const char *ROTATE = "rotate";
static const char *FRAMERATE = "framerate";
static const char *CHAPTER_START_TIME = "chapter_start_time";
static const char *CHAPTER_END_TIME = "chapter_end_time";
static const char *CHAPTER_COUNT = "chapter_count";
static const char *FILESIZE = "filesize";
static const char *VIDEO_WIDTH = "video_width";
static const char *VIDEO_HEIGHT = "video_height";


namespace fmmr
{
void set_duration(AVFormatContext* ic, AVStream* avstream);
void set_framerate(AVFormatContext* ic, AVStream* avsteram);
void set_filesize(AVFormatContext* ic);
void set_rotation(AVFormatContext* ic, AVStream* avstream);
void set_video_dimensions(AVFormatContext *ic, AVStream *avstream);
void set_shutcast_metadata(AVFormatContext* ic);
}; // namespace fmmr

#endif // FFMPEG_FFMPEG_UTILS_H_
