add_rules("mode.debug", "mode.release")
add_repositories("local-repo third_party")
add_requires("local-repo@ffmpeg", {system = false})
add_requires("opencv")

target("fmmr")
	set_kind("shared")
	add_includedirs("include")
	add_files("src/ffmpeg_utils.cpp",
	          "src/mediametadataretriever.cpp",
		  "src/mediametadataretriever_impl.cpp")

	add_packages("ffmpeg")
	add_links("avformat", "avutil", "avcodec", "swscale")
target_end("fmmr")

target("fmmr_test")
	add_deps("fmmr")
	set_kind("binary")
	add_includedirs("include")
	add_files("src/fmmr_test.cpp")
	add_packages("opencv")
	add_links("fmmr", "opencv_core")
target_end("fmmr_test")
